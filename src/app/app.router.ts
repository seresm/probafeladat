import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import { CordToAdressComponent } from './cord-to-adress/cord-to-adress.component';
import { AddresstocordComponent } from './addresstocord/addresstocord.component';

export const router : Routes=[
    {path: '', redirectTo:'cord-to-adress',pathMatch:'full'},
    {path: 'cord-to-adress',component: CordToAdressComponent },
    {path: 'addresstocord',component: AddresstocordComponent }
];

export const routes : ModuleWithProviders = RouterModule.forRoot(router);