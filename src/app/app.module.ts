import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { CordToAdressComponent } from './cord-to-adress/cord-to-adress.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddresstocordComponent } from './addresstocord/addresstocord.component';
import { routes } from './app.router';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    CordToAdressComponent,
    AddresstocordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
