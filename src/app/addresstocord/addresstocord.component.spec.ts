import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddresstocordComponent } from './addresstocord.component';

describe('AddresstocordComponent', () => {
  let component: AddresstocordComponent;
  let fixture: ComponentFixture<AddresstocordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddresstocordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddresstocordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
