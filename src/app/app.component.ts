import { Component } from '@angular/core';
import { CordToAdressComponent } from './cord-to-adress/cord-to-adress.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CordinateAndAdress';
}
