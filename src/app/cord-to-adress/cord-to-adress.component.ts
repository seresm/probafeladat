import { Component, OnInit } from '@angular/core';

export class CordToAdress{
    leiras : String;
    szelesseg: number;
    hosszusag: number;
}

@Component({
  selector: 'app-cord-to-adress',
  templateUrl: './cord-to-adress.component.html',
  styleUrls: ['./cord-to-adress.component.css']
})
export class CordToAdressComponent implements OnInit {

    leirasModel : String;
    szelessegModel : number;
    hosszusagModel : number;
    ctad : CordToAdress[];

  
  constructor() {
      this.leirasModel='';
      this.szelessegModel=0;
      this.hosszusagModel=0;

      let defaultCordToAdress : CordToAdress = {
        leiras: 'Default Cord to Adress',
        szelesseg: 10,
        hosszusag : 10
      };
      this.ctad=[defaultCordToAdress];
   }

  ngOnInit() {
  }
  createCordToAdress(){
      let newCordToAdress : CordToAdress = {
        leiras: this.leirasModel,
        szelesseg : this.szelessegModel,
        hosszusag: this.hosszusagModel
      };
      this.ctad.push(newCordToAdress);
      /*this.leirasModel='';
      this.szelessegModel=this.hosszusagModel=0;*/
  }
}
