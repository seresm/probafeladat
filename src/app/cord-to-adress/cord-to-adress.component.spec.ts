import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CordToAdressComponent } from './cord-to-adress.component';

describe('CordToAdressComponent', () => {
  let component: CordToAdressComponent;
  let fixture: ComponentFixture<CordToAdressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CordToAdressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CordToAdressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
